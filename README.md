# WUIEngine

A multiplayer game maker im working on.

Current features:
Can add objects with a texture and move them around on the client and it will move for everyone.

## Build instructions
TODO. if you need them and they aren't here send me a message on @potatoxel:matrix.org and @potatoxel:potatoxel.org.

## License
    A multiplayer game engine
    Copyright (C) 2021 potatoxel <contact info at potatoxel.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, version 3 of the license only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.